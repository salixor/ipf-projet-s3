OCAMLC=ocamlc
OCAMLOPT=ocamlopt
OCAMLDEP=ocamldep
PDFLATEX=pdflatex

INCLUDES= # includes externes
OCAMLCFLAGS=$(INCLUDES) # flags pour ocamlc
OCAMLOPTFLAGS=$(INCLUDES) # flags pour ocamlopt
PDFLATEXFLAGS=-halt-on-error # flags pour pdflatex

################################################################################

# Tests Graph
TEST_GRAPH_INTERFACES=
TEST_GRAPH_SOURCES=Graph.ml Graph_test.ml

test_graph: $(TEST_GRAPH_INTERFACES) $(TEST_GRAPH_SOURCES:.ml=.cmo)
	$(OCAMLC) -o test_graph $(OCAMLCFLAGS) $(TEST_GRAPH_SOURCES:.ml=.cmo)
test_graph_native: $(TEST_GRAPH_INTERFACES) $(TEST_GRAPH_SOURCES:.ml=.cmx)
	$(OCAMLOPT) -o test_graph.opt $(OCAMLOPTFLAGS) $(TEST_GRAPH_SOURCES:.ml=.cmx)

# Tests WeightedGraph
TEST_WEIGHTEDGRAPH_INTERFACES=
TEST_WEIGHTEDGRAPH_SOURCES=WeightedGraph.ml WeightedGraph_test.ml

test_weightedgraph: $(TEST_WEIGHTEDGRAPH_INTERFACES) $(TEST_WEIGHTEDGRAPH_SOURCES:.ml=.cmo)
	$(OCAMLC) -o test_weightedgraph $(OCAMLCFLAGS) $(TEST_WEIGHTEDGRAPH_SOURCES:.ml=.cmo)
test_weightedgraph_native: $(TEST_WEIGHTEDGRAPH_INTERFACES) $(TEST_WEIGHTEDGRAPH_SOURCES:.ml=.cmx)
	$(OCAMLOPT) -o test_weightedgraph.opt $(OCAMLOPTFLAGS) $(TEST_WEIGHTEDGRAPH_SOURCES:.ml=.cmx)

# Tests
tests: test_graph test_weightedgraph
tests_native: test_graph_native test_weightedgraph_native

################################################################################

# Phase 1
PHASE1_INTERFACES=Analyse.mli
PHASE1_SOURCES=WeightedGraph.ml ModuleGraph.ml Analyse.ml Phase1.ml Phase1Exec.ml

phase1: $(PHASE1_INTERFACES) $(PHASE1_SOURCES:.ml=.cmo)
	$(OCAMLC) -o phase1 $(OCAMLCFLAGS) $(PHASE1_SOURCES:.ml=.cmo)
phase1_native: $(PHASE1_INTERFACES) $(PHASE1_SOURCES:.ml=.cmx)
	$(OCAMLOPT) -o phase1.opt $(OCAMLOPTFLAGS) $(PHASE1_SOURCES:.ml=.cmx)

# Phase 2
PHASE2_INTERFACES=Analyse.mli
PHASE2_SOURCES=WeightedGraph.ml ModuleGraph.ml Analyse.ml Phase2.ml Phase2Exec.ml

phase2: $(PHASE2_INTERFACES) $(PHASE2_SOURCES:.ml=.cmo)
	$(OCAMLC) -o phase2 $(OCAMLCFLAGS) $(PHASE2_SOURCES:.ml=.cmo)
phase2_native: $(PHASE2_INTERFACES) $(PHASE2_SOURCES:.ml=.cmx)
	$(OCAMLOPT) -o phase2.opt $(OCAMLOPTFLAGS) $(PHASE2_SOURCES:.ml=.cmx)

# Phase 3
PHASE3_INTERFACES=Analyse.mli
PHASE3_SOURCES=WeightedGraph.ml ModuleGraph.ml Analyse.ml Phase1.ml Phase2.ml Phase3.ml Phase3Exec.ml

phase3: $(PHASE3_INTERFACES) $(PHASE3_SOURCES:.ml=.cmo)
	$(OCAMLC) -o phase3 $(OCAMLCFLAGS) $(PHASE3_SOURCES:.ml=.cmo)
phase3_native: $(PHASE3_INTERFACES) $(PHASE3_SOURCES:.ml=.cmx)
	$(OCAMLOPT) -o phase3.opt $(OCAMLOPTFLAGS) $(PHASE3_SOURCES:.ml=.cmx)

# Compilation des différentes phases
phases: phase1 phase2 phase3

################################################################################

# Rapport
RAPPORT_TEX=rapport/rapport.tex
RAPPORT_DIR=rapport/

rapport.pdf:
	$(PDFLATEX) $(PDFLATEXFLAGS) -output-directory $(RAPPORT_DIR) $(RAPPORT_TEX)
rapport: rapport.pdf

# Compilation des éléments importants
all: rapport.pdf phase1 phase2 phase3

################################################################################

# Règles de compilation exécutées à chaque make
.SUFFIXES: .ml .mli .cmo .cmi .cmx

.ml.cmo:
	$(OCAMLC) $(OCAMLCFLAGS) -c $<

.mli.cmi:
	$(OCAMLC) $(OCAMLCFLAGS) -c $<

.ml.cmx:
	$(OCAMLOPT) $(OCAMLOPTFLAGS) -c $<

.depend:
	$(OCAMLDEP) $(INCLUDES) *.mli *.ml > .depend

################################################################################

# Nettoyage des fichiers temporaires
clean:
	rm -f *.cm[iox]
	rm -f *.o
	rm -f test_graph{,.opt,.exe}
	rm -f test_weightedgraph{,.opt,.exe}
	rm -f phase1{,.opt,.exe}
	rm -f phase2{,.opt,.exe}
	rm -f phase3{,.opt,.exe}
	rm -f rapport/*{.aux,.log,.out,.toc}

include .depend
