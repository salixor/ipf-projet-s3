(* Module Graph *)

module type OrderedType = sig
    type t
    val compare : t -> t -> int
end

module type S = sig
    type node
    module NodeSet : Set.S with type elt = node
    module NodeMap : Map.S with type key = node
    type graph

    val empty : graph
    val is_empty : graph -> bool

    val add_node : node -> graph -> graph
    val remove_node : node -> graph -> graph

    val add_edge : node -> node -> graph -> graph
    val remove_edge : node -> node -> graph -> graph

    val fold_node : (node -> 'a -> 'a) -> graph -> 'a -> 'a
    val fold_edge : (node -> node -> 'a -> 'a) -> graph -> 'a -> 'a

    val nodes : graph -> node list
    val edges : graph -> (node * node) list
    val succs : node -> graph -> NodeSet.t
end

module Make (Ord : OrderedType) : (S with type node = Ord.t) = struct
    (* La structure de graphe est ici une association du type :
            noeud source => ensemble de successeurs *)
    type node = Ord.t
    module NodeSet = Set.Make(Ord)
    module NodeMap = Map.Make(Ord)
    type graph = NodeSet.t NodeMap.t

    (* Le graphe vide est une association vide (pas de noeuds) *)
    let empty = NodeMap.empty

    (* Test de vacuité du graphe [g] : association vide ou non ? *)
    let is_empty g = NodeMap.is_empty g

    (* Ajout d'un noeud [n] au graphe [g] :
        - si il existe déjà une association [n] => ensemble quelconque de noeuds,
          on renvoie [g]
        - sinon, on ajoute une association [n] => ensemble vide *)
    let add_node n g =
        if NodeMap.mem n g then g
        else NodeMap.add n NodeSet.empty g

    (* Retrait d'un noeud [n] au graphe [g] :
         On récupère d'abord l'association sans le noeud [n].
         Ensuite, on va retirer le noeud [m] partout là où il est successeur d'un
         noeud : pour celà, on rajoute progressivement au graphe vide les
         associations existantes en retirant [n] des successeurs. *)
    let remove_node n g =
        let g_without_n_and_its_succs = NodeMap.remove n g in
        NodeMap.fold (
            fun m m_succs g_prov ->
            NodeMap.add m (NodeSet.remove n m_succs) g_prov
        ) g_without_n_and_its_succs empty

    (* Ajout d'une arête orientée de [src] vers [dst] au graphe [g] :
         Tout d'abord, on essaye de trouver une association [src] vers des noeuds,
         en récupérant l'ensemble des noeuds successeurs, ou bien un ensemble vide.
         Ensuite, on ajoute la destination [dst] à cet ensemble de successeurs.
         Puis on crée un nouveau graphe g2 depuis [g] en ajoutant une association
         de [src] vers ce nouvel ensemble de successeurs (y compris [dst]).
         Enfin, on renvoie le nouveau graphe en ajoutant le noeud [dst]
         ie. en créant une association de [dst] vers un ensemble vide. *)
    let add_edge src dst g =
        let src_succs =
            try NodeMap.find src g
            with Not_found -> NodeSet.empty in
        let src_succs_with_dist = NodeSet.add dst src_succs in
        let g2 = NodeMap.add src src_succs_with_dist g in
        add_node dst g2

    (* Suppression d'une arête orientée de [src] vers [dst] au graphe [g] :
         Tout d'abord, on récupère les successeurs du noeud [src] dans [g].
         Ensuite, on stocke l'ensemble des successeurs de [src] en retirant le
         noeud [dst].
         Enfin, on modifie la liste des successeurs de [src] dans [g] avec
         ce nouvel ensemble de successeurs (sans [dst]).
         Si jamais [src] n'a pas de successeurs, on renvoie [g]. *)
    let remove_edge src dst g =
        try
            let src_succs = (NodeMap.find src g) in
            let src_succs_without_dist = NodeSet.remove dst src_succs in
            NodeMap.add src src_succs_without_dist g
        with Not_found -> g

    (* Fold la fonction [f] au graphe [g] avec la valeur [v0] au début
         Ici, le fold a lieu sur les noeuds, donc on applique la fonction aux
         clés de l'association utilisée pour le graphe *)
    let fold_node f g v0 = NodeMap.fold (fun n n_succs acc -> f n acc) g v0

    (* Fold la fonction [f] au graphe [g] avec la valeur [v0] au début
         Ici, le fold a lieu sur les arêtes : pour chaque clé de l'association,
         on va appliquer un fold sur l'ensemble des successeurs de ce noeud *)
    let fold_edge f g v0 =
        NodeMap.fold (
            fun src src_succs acc -> NodeSet.fold (fun dst acc_src -> f src dst acc_src) src_succs acc
        ) g v0

    (* Renvoie la liste des noeuds du graphe [g] *)
    let nodes g =
        fold_node (fun n acc -> n::acc) g []

    (* Renvoie la liste des arêtes du graphe [g] *)
    let edges g =
        fold_edge (fun n_src n_dest acc -> (n_src, n_dest)::acc) g []

    (* Renvoie la liste des successeurs du noeud [n] dans le graphe [g] *)
    let succs n g = NodeMap.find n g;;
end
