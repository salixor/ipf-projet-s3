(* ========================================================================== *)
(* ================== Fonctions de traitement des fichiers ================== *)
(* ========================================================================== *)

(* Exporte les informations du fichier [fn] sous forme d'un graphe et d'un
   couple du module de départ et du module d'arrivée *)
let file_to_structure fn =
    let (transitions, (deb, fin)) = Analyse.analyse_file_1 fn in
    (List.fold_left (
        fun g (src, dst, weight) -> ModuleGraph.Graph.add_undirected_edge src dst weight g
    ) ModuleGraph.Graph.empty transitions), (deb, fin);;

(* Créé un graphe en lisant le fichier [fn] suivant le format spécifié dans l'énoncé *)
let file_to_graph fn = fst (file_to_structure fn);;

(* Récupère le départ et la destination souhaitées en lisant le fichier [fn] *)
let file_to_depart_et_destination fn = snd (file_to_structure fn);;



(* ========================================================================== *)
(* ================== Fonctions de traitement des données =================== *)
(* ========================================================================== *)

module DistancesMap = Map.Make(String);; (* Association d'un noeud à sa distance minimale depuis [src] *)
module PredecesseursMap = Map.Make(String);; (* Association d'un noeud à son prédécesseur *)
module NoeudsNonTraites = Set.Make(String);; (* Ensemble des noeuds non traités *)

(* @requires: [g] un graphe pondéré proprement défini, [src] un noeud du graphe
    @ensures: renvoie l'association [distances] associant à un noeud la distance
              depuis [src] est initialisée de sorte que la distance d'un noeud
              à [src] soit nulle pour [src] et infinie pour tout autre noeud

   Cette fonction procède en associant à chaque noeud du graphe [g] la distance
   initiale adéquate en l'ajoutant à l'association des distances depuis [src] *)
let initialisation g src =
    ModuleGraph.Graph.fold_node (
        fun s acc ->
            if s = src then (DistancesMap.add s 0 acc)
            else (DistancesMap.add s max_int acc)
    ) g DistancesMap.empty;;

(* @requires: [non_traites] est l'ensemble des noeuds non traités jusqu'à
              présent, et l'association [distances] doit contenir une entrée
              pour chaque noeud présent dans [non_traites]
    @ensures: renvoie le noeud dont la distance à [src] est minimale parmi
              l'ensemble [non_traites] des noeuds non traités
     @raises: Not_found si l'association [distances] ne contient pas l'un des
              noeuds non traités de [non_traites]

   Cette fonction procède en itérant sur les noeuds non traités de [non_traites]
   et en récupérant le noeud dont la distance à [src] est la plus faible *)
let trouve_min non_traites distances =
    fst (NoeudsNonTraites.fold (
        fun s (sommet, mini) ->
            let distance_s = (DistancesMap.find s distances) in
            if distance_s < mini then (s, distance_s) else (sommet, mini)
    ) non_traites ("-1", max_int));;

(* @requires: [s1] et [s2] sont des noeuds de [g], et l'association [distances]
              doit contenir une entrée pour [s1] et [s2]
    @ensures: si le passage par le noeud [s1] permet de réduire la distance de
              [src] à [s2], renvoie l'association [distances] et [pred] mises à
              jour, sinon renvoie [distances] et [pred] inchangées
     @raises: Not_found si l'association [distances] ou le graphe [g] ne
              contient pas l'un des deux noeuds [s1] ou [s2]

   Cette fonction procède en récupérant la distance de [src] à [s1] et [s2],
   puis en calculant la nouvelle distance de [src] à [s2] si le chemin passait
   par [s1] pour décider d'ajouter ce noeud au chemin ou non *)
let maj_distances s1 s2 distances pred g =
    let d_s1 = DistancesMap.find s1 distances in
    let d_s2 = DistancesMap.find s2 distances in
    let d_s2_par_s1 = d_s1 + (ModuleGraph.Graph.NodeMap.find s2 (ModuleGraph.Graph.succs s1 g)) in
        if d_s2 > d_s2_par_s1 then (DistancesMap.add s2 d_s2_par_s1 distances, PredecesseursMap.add s2 s1 pred)
        else (distances, pred);;

(* @requires: [src] et [s] sont des noeuds du graphe [g], et le noeud [s] est
              présent dans l'association [pred] des prédecesseurs
    @ensures: renvoie la liste des noeuds de [s] à [src] en se référant aux
              prédécesseurs fournis par [pred]
     @raises: Not_found si [s] n'existe pas dans l'association [pred]

   Cette fonction procède récursivement en ajoutant [s] à la liste actuelle puis
   en récupérant le prédécesseur de [s] pour trouver son prédecesseur et ainsi
   de suite jusqu'à ce que [s] soit [src] lui-même (fin du chemin) *)
let rec pred_to_chemin pred s src acc =
    if s = src then src::acc
    else s::(pred_to_chemin pred (PredecesseursMap.find s pred) src acc);;

(* @requires: [src] et [dst] sont des noeuds de [g] et [g] est un graphe pondéré
              proprement défini
    @ensures: renvoie la distance minimale de [src] à [dst] ainsi que le chemin
              parcouru de [src] à [dst] sous forme de liste de noeuds
     @raises: Not_found si [src] ou [dst] n'est pas présent dans le graphe [g]

   Cette fonction procède tout d'abord en initialisant l'association [distances]
   puis en itérant sur chaque noeud non traité, en récupérant à chaque fois le
   noeud [s1] dont la distance à [src] est la plus faible, puis en retirant ce
   noeud des noeuds non traités avant de mettre à jour les distances et
   prédécesseurs pour tous les voisins de [s1]
   On "arrête" l'algorithme dès que [s1] est égal à [dst] *)
let dijkstra g src dst =
    let non_traites_initial = ModuleGraph.Graph.fold_node (
        fun n acc -> NoeudsNonTraites.add n acc
    ) g NoeudsNonTraites.empty in
    let distances_initial = (initialisation g src) in
    let (_, distances, pred, _) = NoeudsNonTraites.fold (
        fun _ (non_traites, distances, pred, fini) ->
            if fini then (non_traites, distances, pred, fini)
            else
                let s1 = trouve_min non_traites distances in
                let s1_succs = ModuleGraph.Graph.succs s1 g in
                let non_traites_prive_de_s1 = NoeudsNonTraites.remove s1 non_traites in
                let (distances_updated, pred_updated) = ModuleGraph.Graph.NodeMap.fold (
                    fun s2 w (acc_distances, pred) -> maj_distances s1 s2 acc_distances pred g
                ) s1_succs (distances, pred) in
                (non_traites_prive_de_s1, distances_updated, pred_updated, (s1 = dst))
    ) non_traites_initial (non_traites_initial, distances_initial, PredecesseursMap.empty, false)
    in (DistancesMap.find dst distances, List.rev (pred_to_chemin pred dst src []));;



(* ========================================================================== *)
(* ========================= Fonctions d'affichages ========================= *)
(* ========================================================================== *)

(* Fonction d'affichage de la solution finale étant donné un fichier *)
let affichage_phase1 fn =
    let (g, (src, dst)) = (file_to_structure fn) in
    print_string ("* Solution pour la phase 1 ("^src^" -> "^dst^") : ");
    if src = dst then print_endline ("départ et destination identiques !")
    else try
        let (time, modules) = (dijkstra g src dst) in
        match modules with
            | [] -> print_endline ("aucun chemin ne permet ce trajet !")
            | modules -> print_newline (); Analyse.output_sol_1 time modules
    with
        | Not_found -> print_endline ("aucun chemin ne permet ce trajet !")
        | _ -> print_endline ("erreur non gérée …");;
