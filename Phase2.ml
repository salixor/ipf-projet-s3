(* ========================================================================== *)
(* ================== Fonctions de traitement des fichiers ================== *)
(* ========================================================================== *)

(* Exporte les informations du fichier [fn] sous forme d'un graphe et d'une
   liste des chemins des utilisateurs *)
let file_to_structure fn =
    let (transitions, chemins_utilisateurs) = Analyse.analyse_file_2 fn in
    let g = (List.fold_left (
        fun g (src, dst, weight) -> ModuleGraph.Graph.add_undirected_edge src dst weight g
    ) ModuleGraph.Graph.empty transitions) in
    let chemins = List.fold_left (
        fun acc_chemins chemin ->
            (ModuleGraph.liste_noeuds_convert chemin g)::acc_chemins
    ) [] chemins_utilisateurs in (g, chemins);;

(* Créé un graphe en lisant le fichier [fn] suivant le format spécifié dans l'énoncé *)
let file_to_graph fn = fst (file_to_structure fn);;

(* Récupère la liste des chemins des utilisateurs en lisant le fichier [fn] *)
let file_to_chemins fn = snd (file_to_structure fn);;



(* ========================================================================== *)
(* ================== Fonctions de traitement des données =================== *)
(* ========================================================================== *)

(* Module définissant une association d'un arc vers le temps de début minimal
   pour cet arc, permettant de garder la valeur du dernier temps de début pour
   chaque arc *)
module ArcDerniereOccupation = Map.Make(
    struct
        type t = (ModuleGraph.Graph.node * ModuleGraph.Graph.node)
        let compare = compare
    end);;

(* @requires: [chemins] la liste des chemins des utilisateurs avant tout
              traitement, après appel à la fonction d'analyse du fichier,
              sous forme (n_src, n_dst, weight)
    @ensures: renvoie une liste de couples dont le premier élément est une liste
              correspondant à la partie de chemin non traitée pour chaque
              utilisateur, et le second élément est une liste coresspondant à la
              partie du chemin traitée, sous forme [(n_src, n_dst, weight, t_deb)],
              et le second élément est l'association des arcs et temps de début
              minimal

   Cette fonction initialise le couple (parties de chemins non traitées, parties
   de chemins traitées) pour chacun des utilisateurs *)
let initialisation chemins =
    (List.fold_left (
        fun acc_chemins_avec_traites chemin ->
            (chemin, [])::acc_chemins_avec_traites
    ) [] chemins), ArcDerniereOccupation.empty;;

(* @requires: [chemins_init] une liste de couples (parties de chemins non
              traitées, parties de chemins traitées), comme récupéré à l'issue
              de la fonction initialisation
    @ensures: renvoie la valeur de l'arc de coût maximal dans l'ensemble des
              parties de chemins non traitées

   Cette fonction procède en itérant sur la liste des couples, en cherchant
   le poids maximal de la première arête de chacun des chemins non traités
   pour tous les utilisateurs *)
let find_max_cost chemins_init = List.fold_left (
        fun poids_max (chemin_non_traite, _) -> List.fold_left (
            fun poids_max (_, _, w) -> (max w poids_max)
        ) poids_max chemin_non_traite
    ) 0 chemins_init;;

(* @requires: [chemin_traite] le chemin d'un utilisateur après qu'il ait été
              entièrement traité
    @ensures: renvoie la durée totale de ce chemin

   Cette fonction va calculer la durée totale du chemin en prenant en compte
   la durée entre chaque module ainsi que les temps de début pour chacun des
   modules *)
let duree_totale_chemin chemin_traite = List.fold_left (
        fun t_max (_, _, w, t_deb) -> let nv_t = t_deb + w in (max nv_t t_max)
    ) 0 chemin_traite;;

(* @requires: [chemins_init] correspond à la liste de couples décrite précédemment
              qui a été initialisée via un appel à initialisation au tout début
              et [occupations] est l'association des arêtes et du temps minimal
              de début de cette arête avant le traitement de cette étape
    @ensures: renvoie la nouvelle liste de couples des parties de chemins traitées
              et non traitées après avoir réalisé une étape de plus dans le
              traitement de l'ensemble des chemins *)
let traitement_une_etape chemins_init occupations =
    let poids_plus_long = find_max_cost chemins_init in
    List.fold_left (
        fun (acc, acc_occupations) (chemin, chemin_traite) ->
            let (_, nouveau_chemin, nouveau_traite, occupations) = (List.fold_left (
                fun (actuel_poids, acc_nouveau_chemin, acc_nouveau_traite, occupations) (n_pred, n_succ, w) ->
                    (* Récupération du temps de début de l'arête précédente pour ce chemin *)
                    let temps_debut_precedent =
                        try let (_, _, w, temps_debut) = (List.hd (acc_nouveau_traite@chemin_traite)) in w + temps_debut
                        with _ -> 0 in
                    (* Calcul du nouveau temps de début : soit c'est le temps de
                       début minimal pour cette arête, soit c'est le temps de
                       début précédent *)
                    let nouveau_temps_debut =
                        try max (ArcDerniereOccupation.find (n_pred, n_succ) occupations) temps_debut_precedent
                        with _ -> try max (ArcDerniereOccupation.find (n_succ, n_pred) occupations) temps_debut_precedent
                        with _ -> temps_debut_precedent in
                    (* Si le poids de l'arête et la somme des poids ajoutés
                       jusqu'à présent sont plus faible que le poids du plus
                       grand arc parmi les chemins traités, alors on l'ajoute
                       à la partie traitée *)
                    if w + actuel_poids <= poids_plus_long then
                        let nouvelles_occupations = ArcDerniereOccupation.add (n_pred, n_succ) (nouveau_temps_debut + w + actuel_poids) occupations in
                        let nouvelles_occupations2 = ArcDerniereOccupation.add (n_succ, n_pred) (nouveau_temps_debut + w + actuel_poids) nouvelles_occupations in
                        (w + actuel_poids, acc_nouveau_chemin, (n_pred, n_succ, w, nouveau_temps_debut)::acc_nouveau_traite, nouvelles_occupations2)
                    else
                        (actuel_poids, (n_pred, n_succ, w)::acc_nouveau_chemin, acc_nouveau_traite, occupations)
            ) (0, [], [], acc_occupations) chemin) in
            (List.rev nouveau_chemin, nouveau_traite@chemin_traite)::acc, occupations
    ) ([], occupations) chemins_init;;

(* @requires: [chemins] correspond à la liste des chemins des utilisateurs
    @ensures: renvoie la liste des chemins convertie sous le format utilisé
              pour l'affichage de la solution de la phase 2 ainsi que la
              durée totale maximale pour que tous les utilisateurs
              effectuent leur chemin en entier *)
let traitement_complet chemins =
    (* Réalise les itérations sur le chemin tant qu'il reste des parties de
       chemin non traitées *)
    let rec aux (chemins_convertis, occupations) =
        let fini = List.for_all (fun (chemin, chemin_traite) -> chemin = []) chemins_convertis in
        if fini then (chemins_convertis, occupations)
        else aux (traitement_une_etape chemins_convertis occupations)
    in let (chemins_convertis_et_traites, _) = aux (initialisation chemins)
    (* Calcule la durée totale de parcours de l'ensemble des chemins étant
       donné les temps de départ trouvés *)
    in let cout_maximal = List.fold_left (
        fun duree_max (_, chemin_traite) ->
            max (duree_totale_chemin chemin_traite) duree_max
    ) 0 chemins_convertis_et_traites
    (* Conversion sous forme attendue par la fonction d'affichage de la phase 2 *)
    in let converti_to_ouputable_phase_2 = List.rev (List.fold_left (
        fun acc (chemin, chemin_traite) ->
            let (noeud_fin, _, _, _) = (List.hd (List.rev chemin_traite)) in
            let (noeuds, temps) = (List.fold_left (
                fun (noeuds, temps) (_, n_succ, _, t_deb) -> (n_succ::noeuds, t_deb::temps)
            ) ([noeud_fin], []) (List.rev chemin_traite)) in
            (List.rev noeuds, List.rev temps)::acc
    ) [] chemins_convertis_et_traites)
    in (converti_to_ouputable_phase_2, cout_maximal);;



(* ========================================================================== *)
(* ========================= Fonctions d'affichages ========================= *)
(* ========================================================================== *)

(* Fonction d'affichage de la solution finale étant donné un fichier *)
let affichage_phase2 fn =
    let chemins_a_traiter = (file_to_chemins fn) in
    let (solution, duree_maximale) = (traitement_complet chemins_a_traiter) in
    print_endline ("* Solution pour la phase 2 : ");
    Analyse.output_sol_2 solution;
    print_int duree_maximale;;
