(* ========================================================================== *)
(* ========================= Définitions des types ========================== *)
(* ========================================================================== *)

(* Module de graphes avec poids, où les noeuds sont des chaînes de caractères
   et les poids sont des entiers *)
module Int : (WeightedGraph.OrderedType with type t = int) = struct
    type t = int
    let compare = compare
end
module Graph = WeightedGraph.Make (String) (Int);;



(* ========================================================================== *)
(* ================== Fonctions de traitement des données =================== *)
(* ========================================================================== *)

(* Converti une liste de noeuds [n_1; n_2; …; n_i] en une liste de la
   forme [(n_1, n_2, w_1_2); …; (n_i-1, n_i, w_i-1_i)] *)
let liste_noeuds_convert chemin g = List.rev (snd (List.fold_left (
        fun (n_pred, acc_chemin) n_succ ->
            let poids_arete = Graph.NodeMap.find n_succ (Graph.succs n_pred g) in
            (n_succ, (n_pred, n_succ, poids_arete)::acc_chemin)
    ) (List.hd chemin, []) (List.tl chemin)));;



(* ========================================================================== *)
(* ========================= Fonctions d'affichages ========================= *)
(* ========================================================================== *)

(* Affiche le graphe, en affichant les noeuds puis les arcs avec les poids *)
let output_graph g =
    let nodes_to_string = Graph.fold_node (fun n acc -> n::acc) g [] in
    let edges_to_string = Graph.fold_undirected_edge (
        fun src dst w acc -> (src^" - "^dst^" [Poids : "^string_of_int(w)^"]")::acc
    ) g [] in
    print_string "* Noeuds du graphe [g] : ";
    List.iter (fun s -> print_string (s^" ")) (List.rev nodes_to_string); print_newline (); print_newline ();
    print_endline "* Arcs du graphe [g] : ";
    List.iter (fun s -> print_endline s) (List.rev edges_to_string);;

(* Affiche les chemins des utilisateurs *)
let output_chemins chemins =
    print_endline "* Chemins des utilisateurs : ";
    List.iter (
        fun chemin ->
            let (src, _, _) = List.hd chemin in print_string src; List.iter (
                fun (_, n_succ, w) -> print_string (" ["^string_of_int(w)^"]-> "^n_succ)
            ) chemin; print_newline ();
    ) (List.rev chemins);;
