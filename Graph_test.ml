(* Tests du module Graph avec le type String comme noeuds *)

module MyStringGraph = Graph.Make(String);;

print_string "Testing string labelled graphs."; print_newline();;

let test_g = MyStringGraph.empty;;
MyStringGraph.is_empty test_g;;
MyStringGraph.nodes test_g;;

let test_g2 = MyStringGraph.add_node "test" test_g;;
MyStringGraph.is_empty test_g2;;
MyStringGraph.succs "test" test_g2;;
MyStringGraph.nodes test_g2;;

let test_g3 = MyStringGraph.remove_node "test" test_g2;;
MyStringGraph.is_empty test_g3;;
MyStringGraph.nodes test_g3;;

let test_g4 = MyStringGraph.add_edge "test" "destination" test_g2;;
MyStringGraph.is_empty test_g4;;
MyStringGraph.succs "test" test_g4;;
MyStringGraph.nodes test_g4;;
MyStringGraph.edges test_g4;;

let test_g5 = MyStringGraph.remove_edge "test" "destination" test_g4;;
MyStringGraph.is_empty test_g5;;
MyStringGraph.succs "test" test_g5;;
MyStringGraph.nodes test_g5;;
MyStringGraph.edges test_g5;;

let full_test = MyStringGraph.add_edge "test" "destination_1" (MyStringGraph.add_edge "test" "destination_2" MyStringGraph.empty);;
let full_test = MyStringGraph.add_edge "destination_1" "destination_1_1" (MyStringGraph.add_edge "destination_1" "destination_1_2" full_test);;
MyStringGraph.succs "test" full_test;;
MyStringGraph.nodes full_test;;
MyStringGraph.edges full_test;;

let nodes_to_string = MyStringGraph.fold_node (fun n acc -> n::acc) full_test [];;
print_string "Nodes : "; print_newline ();;
List.iter (fun s -> print_string (" - "^s); print_newline ()) nodes_to_string; print_newline ();;

let edges_to_string = MyStringGraph.fold_edge (fun n_src n_dest acc -> (n_src^" -> "^n_dest)::acc) full_test [];;
print_string "Edges : "; print_newline ();;
List.iter (fun s -> print_string (" - "^s); print_newline ()) edges_to_string;;
