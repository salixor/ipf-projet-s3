(* Module WeightedGraph *)

module type OrderedType = sig
    type t
    val compare : t -> t -> int
end

module type S = sig
    type node
    type weight
    module NodeMap : Map.S with type key = node
    module GraphMap : Map.S with type key = node
    type graph = (weight NodeMap.t) GraphMap.t

    val empty : graph
    val is_empty : graph -> bool

    val add_node : node -> graph -> graph
    val remove_node : node -> graph -> graph

    val add_edge : node -> node -> weight -> graph -> graph
    val add_undirected_edge : node -> node -> weight -> graph -> graph
    val remove_edge : node -> node -> graph -> graph
    val remove_undirected_edge : node -> node -> graph -> graph

    val fold_node : (node -> 'a -> 'a) -> graph -> 'a -> 'a
    val fold_edge : (node -> node -> weight -> 'a -> 'a) -> graph -> 'a -> 'a
    val fold_undirected_edge : (node -> node -> weight -> 'a -> 'a) -> graph -> 'a -> 'a

    val nodes : graph -> node list
    val edges : graph -> (node * node * weight) list
    val succs : node -> graph -> weight NodeMap.t
end

module Make (N : OrderedType) (W : OrderedType) : (S with type node = N.t and type weight = W.t) = struct
    (* La structure de graphe est ici une association:
         source => (association de destination => poids de l'arête) *)
    type node = N.t
    type weight = W.t
    module NodeMap = Map.Make(N)
    module GraphMap = Map.Make(N)
    type graph = (weight NodeMap.t) GraphMap.t

    (* Le graphe vide est une association vide (pas de noeuds) *)
    let empty = GraphMap.empty

    (* Test de vacuité du graphe [g] : association vide ou non ? *)
    let is_empty g = GraphMap.is_empty g

    (* Ajout d'un noeud [n] au graphe [g] :
        - si il existe déjà une association [n] => association non vide de
          noeuds, on renvoie [g]
        - sinon, on ajoute une association [n] => association vide de noeuds *)
    let add_node n g =
        if GraphMap.mem n g then g
        else GraphMap.add n NodeMap.empty g

    (* Retrait d'un noeud [n] au graphe [g] :
         On récupère d'abord l'association sans le noeud [n].
         Ensuite, on va retirer le noeud [m] partout là où il est successeur d'un
         noeud : pour celà, on rajoute progressivement au graphe vide les
         associations existantes en retirant [n] des successeurs. *)
    let remove_node n g =
        let g_without_n_and_its_succs = GraphMap.remove n g in
        GraphMap.fold (
            fun m m_succs g_prov ->
            GraphMap.add m (NodeMap.remove n m_succs) g_prov
        ) g_without_n_and_its_succs empty

    (* Ajout d'une arête orientée de [src] vers [dst] au graphe [g] de poids [weight] :
         Tout d'abord, on essaye de trouver une association [src] vers des noeuds,
         en récupérant l'ensemble des noeuds successeurs, ou bien on la choisit vide.
         Ensuite, on ajoute la destination [dst] à cet ensemble de successeurs,
         en ajoutant le poids.
         Puis on crée un nouveau graphe g2 depuis [g] en ajoutant une association
         de [src] vers ce nouvel ensemble de successeurs (y compris [dst]).
         Enfin, on renvoie le nouveau graphe en ajoutant le noeud [dst]
         ie. en créant une association de [dst] vers un ensemble vide. *)
    let add_edge src dst weight g =
        let src_succs =
            try GraphMap.find src g
            with Not_found -> NodeMap.empty in
        let src_succs_with_dist = NodeMap.add dst weight src_succs in
        let g2 = GraphMap.add src src_succs_with_dist g in
        add_node dst g2

    (* Ajoute une arête non orientée de poids [weight] entre [n1] et [n2] en
       ajoutant les deux arêtes orientées associées au graphe [g] *)
    let add_undirected_edge n1 n2 weight g =
        add_edge n1 n2 weight (add_edge n2 n1 weight g)

    (* Suppression de l'arête orientée de [src] vers [dst] au graphe [g] :
         Tout d'abord, on récupère les successeurs du noeud [src] dans [g].
         Ensuite, on stocke l'ensemble des successeurs de [src] en retirant le
         noeud [dst].
         Enfin, on modifie la liste des successeurs de [src] dans [g] avec
         ce nouvel ensemble de successeurs (sans [dst]).
         Si jamais [src] n'a pas de successeurs, on renvoie [g]. *)
    let remove_edge src dst g =
        try
            let src_succs = (GraphMap.find src g) in
            let src_succs_without_dist = NodeMap.remove dst src_succs in
            GraphMap.add src src_succs_without_dist g
        with Not_found -> g


    (* Suppression de l'arête non orientée entre [n1] et [n2] en supprimant les
       deux arêtes orientées associées au graphe [g] *)
    let remove_undirected_edge n1 n2 g =
        remove_edge n1 n2 (remove_edge n2 n1 g)

    (* Fold la fonction [f] au graphe [g] avec la valeur [v0] au début
         Ici, le fold a lieu sur les noeuds, donc on applique la fonction aux
         clés de l'association utilisée pour le graphe *)
    let fold_node f g v0 = GraphMap.fold (fun n n_succs acc -> f n acc) g v0

    (* Fold la fonction [f] au graphe [g] avec la valeur [v0] au début
         Ici, le fold a lieu sur les arêtes : pour chaque clé de l'association,
         on va appliquer un fold sur l'ensemble des successeurs de ce noeud *)
    let fold_edge f g v0 =
        GraphMap.fold (
            fun src src_succs acc -> NodeMap.fold (
                fun dst dst_weights acc_src -> f src dst (NodeMap.find dst src_succs) acc_src
            ) src_succs acc
        ) g v0

    (* Fold sur les arêtes mais en prenant en compte le caractère non orienté
       afin de ne pas traîter deux fois chaque arête *)
    let fold_undirected_edge f g v0 =
        fst (fold_edge (
            fun src dst w (acc, parcouru) ->
                if (List.mem (src, dst) parcouru) then (acc, parcouru)
                else ( f src dst w acc, (src, dst)::(dst, src)::parcouru )
        ) g (v0, []))

    (* Renvoie la liste des noeuds du graphe [g] *)
    let nodes g =
        fold_node (fun n acc -> n::acc) g []

    (* Renvoie la liste des arêtes (src, dst, weight) du graphe [g] *)
    let edges g =
        fold_edge (fun n_src n_dest weight acc -> (n_src, n_dest, weight)::acc) g []

    (* Renvoie la liste des successeurs du noeud [n] dans le graphe [g] avec le
       poids associé *)
    let succs n g = GraphMap.find n g;;
end
