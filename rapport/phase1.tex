\section{Phase 1}

\subsection{Choix de la structure pour stocker le plan}

Nous disposons pour chaque plan d'un fichier suivant le format suivant :
\begin{itemize}
\item Une ligne contenant un entier \texttt{n} indiquant le nombre de liaisons entre modules
\item \texttt{n} lignes indiquant une liaison entre un premier module et un second module, ainsi que la durée du transit pour cette liaison
\item Une dernière ligne contenant deux noms de modules, le premier étant le module de départ de l'unique individu dans la base et le second la destination
\end{itemize}

Bien évidemment, ce format n'est pas adapté pour traîter les données, et il a donc fallu fournir une structure permettant de stocker le plan en \texttt{OCaml} (la lecture du fichier est possible grâce à la fonction \texttt{analyse\_file\_1 fn} fournie dans le module \texttt{Analyse}).

On remarque assez rapidement que la structure de la base martienne correspond exactement à un graphe non orienté et pondéré :
\begin{itemize}
\item les modules correspondent aux noeuds ou aux sommets du graphe
\item les liaisons entre nos différents modules correspondent à des arêtes pondérées
\end{itemize}
Il a donc fallu trouver une structure qui permette une manipulation facile des données après la conversion sous cette forme.

Dans un premier temps, j'ai songé à utiliser les arcs \texttt{(src, dst)} comme clés d'une association, puis le poids comme valeur. Toutefois, cette structure semblait présenter le défaut d'être difficilement maniable pour accéder aux arcs, et j'ai rapidement abandonné cette première idée.

En cours, nous avions vu une structure de graphe orientés sans pondération, se formant d'une association d'un noeud vers l'ensemble des noeuds successeurs. Cette structure correspond finalement à une liste d'association, ce qui est souvent utilisé en théorie des graphes. Toutefois, la structure vue en cours présente deux problèmes par rapport à la structure souhaitée pour la base martienne :
\begin{itemize}
\item les arêtes sont orientées puisque chaque noeud est associé à ses successeurs
\item les arêtes ne sont pas pondérées
\end{itemize}

Pour résoudre le caractère orienté et disposer d'un graphe non orienté, il suffit d'ajouter une nouvelle fonction \texttt{add\_undirected\_edge src dst g} permettant l'ajout d'arêtes non orientées, en ajoutant les deux arêtes orientées correspondantes (ce qui est exactement ce qui est fait en utilisant des listes d'association pour un graphe non orienté).

Concernant le caractère pondéré du graphe, plusieurs solutions étaient possibles. J'ai personnellement changé l'utilisation d'un \texttt{Set} pour stocker les successeurs d'un noeud par l'utilisation d'une seconde association, où les clés sont les noeuds successeurs et les valeurs le poids de l'arête correspondante. Il aurait aussi été possible de conserver l'utilisation du module \texttt{Set} en stockant des couples \texttt{(noeud\_successeur, poids)}.

Ainsi, on obtient finalement une structure qui va permettre de réaliser facilement des opérations sur les graphes, ce qui va être fortement utile pour la phase 1 mais aussi la phase 2 et la phase 3.

\subsection{Détermination du chemin de coût le plus faible}

On souhaite que notre unique individu puisse se rendre de son point de départ à sa destination en suivant le chemin qui lui coûte le moins de temps. Pour celà, nous devons réaliser une recherche du chemin de coût le plus faible dans un graphe.

Tout d'abord, on se rend bien évidemment rapidement compte qu'il s'agit ici d'utiliser un algorithme de détermination de chemin de coût le plus faible dans un graphe non orienté. Dans la mesure où il s'agit de plus de déterminer le chemin de coût le plus faible étant donné un point de départ, les deux algorithmes intéressants sont l'algorithme de Dijkstra ou l'algorithme de Bellman-Ford. Dans notre problème, puisque nous traitons avec des durées, tous les poids sur les arêtes sont positifs et j'ai donc choisi d'implémenter l'algorithme de Dijkstra.

Dans un premier temps, il s'agit d'initialiser la structure qui va permettre de conserver les distances du point de départ \texttt{src} à chaque noeud du graphe : cette structure sera une association dont les clés sont les noms des noeuds, et les valeurs sont la distance de \texttt{src} au noeud. En particulier, les distances seront initialement à la valeur \texttt{max\_int} pour représenter l'infini, et à la valeur \texttt{0} dans le cas de \texttt{src} (c'est le noeud le plus proche de \texttt{src} …). Cette opération sera réalisée par la fonction \texttt{initialisation g src} qui prend en paramètres le graphe et le noeud de départ, et renvoie l'association tout juste décrite.

La seconde fonction utile est \texttt{trouve\_min q distances} qui permet de trouver le noeud de distance minimale allant de \texttt{src} à un noeud et qui n'a pas encore été traité (\texttt{q} est un ensemble de noeuds non traités). En sortie de cette fonction, le noeud non traité de distance la plus faible par rapport à \texttt{src} est renvoyé.

Pour le moment, les distances ne sont pas jamais mises à jour : c'est le fait de la fonction \texttt{maj\_distances s1 s2 distances pred g} qui va mettre à jour la distance entre \texttt{src} et \texttt{s2}, en s'interrogeant sur la pertinence de parcourir le noeud \texttt{s1} dans le chemin le plus court. Si la distance actuelle de \texttt{src} à \texttt{s2} est plus importante que celle en passant par \texttt{s1} (la distance de \texttt{src} à \texttt{s1} en plus du poids de l'arête entre \texttt{s1} et \texttt{s2}), alors on modifie la distance de \texttt{src} à \texttt{s2} avec cette distance plus faible, et on ajoute \texttt{s1} aux prédecesseurs de \texttt{s2}.

Enfin, il est possible d'effectivement appliquer l'algorithme de Dijkstra \texttt{dijkstra g src dst} : dans un premier temps, l'ensemble des noeuds non traités est créé depuis l'ensemble des noeuds du graphe. Ensuite, l'association des noeuds à leur distance depuis \texttt{src} est initialisée en faisant appel à \texttt{initialisation} et, tant que l'ensemble des noeuds non traités n'est pas vide, le noeud \texttt{s1} de distance minimale à \texttt{src} est trouvé et supprimé de cet ensemble, puis, pour chaque voisin de \texttt{s1}, la distance est mise à jour grâce à \texttt{maj\_distances}.

Enfin, l'algorithme de Dijkstra nous donne accès à la fois au poids du chemin de coût minimal et à la liste des noeuds parcourus de \texttt{src} à \texttt{dst}. Cette liste de noeuds parcourus se récupère grâce à l'association de prédécesseurs, mise à jour lors des appels à la fonction \texttt{maj\_distances} : chaque noeud est associé un prédecesseur.

Il suffit alors de faire appel à \texttt{Analyse.output\_sol\_1} pour afficher la solution dans le format demandé. Après compilation via la commande \texttt{make phase1}, il est possible d'utiliser la commande \texttt{./phase1 sources/1.txt} pour afficher la solution sur l'exemple fourni avec l'énoncé.
