(* ========================================================================== *)
(* ================== Fonctions de traitement des fichiers ================== *)
(* ========================================================================== *)

(* Exporte les informations du fichier [fn] sous forme d'un graphe et d'une
   liste de couples de la forme (mod_depart, mod_arrivee) correspondant aux
   modules de départ et d'arrivée des utilisateurs *)
let file_to_structure fn =
    let (transitions, chemins_utilisateurs) = Analyse.analyse_file_2 fn in
    let g = (List.fold_left (
        fun g (src, dst, weight) -> ModuleGraph.Graph.add_undirected_edge src dst weight g
    ) ModuleGraph.Graph.empty transitions) in
    let liste_departs_arrivees = List.map (
        fun chemin -> match chemin with
            | [] -> failwith "Problème de format du fichier (pas de départ/arrivée)"
            | _::[] -> failwith "Problème de format du fichier (pas de départ/arrivée)"
            | n_src::n_dst::_ -> (n_src, n_dst)
    ) chemins_utilisateurs
    in (g, liste_departs_arrivees);;

(* Créé un graphe en lisant le fichier [fn] suivant le format spécifié dans l'énoncé *)
let file_to_graph fn = fst (file_to_structure fn);;

(* Récupère la liste des départs et arrivées des utilisateurs en lisant le
   fichier [fn] *)
let file_to_liste_departs_arrivees fn = snd (file_to_structure fn);;



(* ========================================================================== *)
(* ================== Fonctions de traitement des données =================== *)
(* ========================================================================== *)

(* Calcule les chemins de durée minimale pour chaque utilisateur connaissant
   leur module de départ et d'arrivée en utilisant la fonction appliquant
   l'algorithme de Dijkstra écrite pour la phase 1 *)
let calcul_chemins liste_departs_arrivees g = List.map (
    fun (depart, arrivee) -> snd (Phase1.dijkstra g depart arrivee)
) liste_departs_arrivees;;

(* Conversion de la liste de liste de noeuds [n_1; n_2; …; n_i] en une liste
   de listes de la forme [(n_1, n_2, w_1_2); …; (n_i-1, n_i, w_i-1_i)] *)
let conversion_chemins liste_chemins g = List.map (
    fun chemin -> ModuleGraph.liste_noeuds_convert chemin g
) liste_chemins;;

(* Détermination des chemins convertis au format (n_src, n_dst, weight) pour
   les utilisateurs en appliquant Dijkstra pour trouver le chemin de durée
   minimale entre le départ et l'arrivée puis en réalisant la conversion *)
let determination_chemins (g, liste_departs_arrivees) =
    let chemins_utilisateurs = (calcul_chemins liste_departs_arrivees g) in
    let chemins_utilisateurs_convertis = (conversion_chemins chemins_utilisateurs g) in
    chemins_utilisateurs_convertis;;



(* ========================================================================== *)
(* ========================= Fonctions d'affichages ========================= *)
(* ========================================================================== *)

(* Fonction d'affichage de la solution finale étant donné un fichier *)
let affichage_phase3 fn =
    let (solution, duree_maximale) = (Phase2.traitement_complet (determination_chemins (file_to_structure fn))) in
    print_endline ("* Solution pour la phase 3 : ");
    Analyse.output_sol_2 solution;
    print_int duree_maximale;;
