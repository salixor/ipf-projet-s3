# Utilisation du projet

Ce projet a été réalisé par Kévin Cocchi dans le cadre de l'UE IPF.

## Compilation

Ce dossier contient un `Makefile` permettant de compiler facilement les fichiers nécessaires aux différentes phases :
- `make phase1` permet de compiler la phase 1
- `make phase2` permet de compiler la phase 2
- `make phase3` permet de compiler la phase 3
- `make phases` permet la compilation de toutes les phases d'un seul coup

## Test d'une phase

Après avoir compilé la phase souhaitée, il est possible de trouver la solution pour une phase étant donné un **fichier au bon format** via la commande suivante :
- pour la **phase 1**, `./phase1 [fn]` affichera le graphe et le plus court chemin de la destination à l'arrivée
- pour la **phase 2**, `./phase2 [fn]` affichera le graphe, les chemins des utilisateurs et la séquence des déplacements pour chaque utilisateur (avec la durée de transit globale)
- pour la **phase 3**, `./phase3 [fn]` affichera le graphe, les chemins des utilisateurs déterminés et la séquence des déplacements pour chaque utilisateur (avec la durée de transit globale)

## Autres compilations

Le `Makefile` permet aussi la compilation de tests sur les graphes et de tests sur les graphes pondérés via les commandes :
- `make test_graph` pour les tests sur les graphes orientés **non pondérés**
- `make test_weightedgraph` pour les tests sur les graphes orientés **pondérés**
Il suffit ensuite de lancer la commande `./test_graph` ou `./test_weightedgraph` pour afficher le résultat.

Il est possible de regénérer le rapport sous version .PDF via la commande `make rapport`.
