(* Tests du module WeightedGraph avec le String pour les noeuds
   et Int pour les poids *)

module Int : (WeightedGraph.OrderedType with type t = int) = struct
    type t = int
    let compare = compare
end

module MyWeightedGraph = WeightedGraph.Make (String) (Int);;

let test_g = MyWeightedGraph.empty;;
MyWeightedGraph.is_empty test_g;;
MyWeightedGraph.nodes test_g;;

let test_g2 = MyWeightedGraph.add_node "test" test_g;;
MyWeightedGraph.is_empty test_g2;;
MyWeightedGraph.succs "test" test_g2;;
MyWeightedGraph.nodes test_g2;;

let test_g3 = MyWeightedGraph.remove_node "test" test_g2;;
MyWeightedGraph.is_empty test_g3;;
MyWeightedGraph.nodes test_g3;;

let test_g4 = MyWeightedGraph.add_edge "test" "destination" 50 test_g2;;
MyWeightedGraph.is_empty test_g4;;
MyWeightedGraph.succs "test" test_g4;;
MyWeightedGraph.nodes test_g4;;
MyWeightedGraph.edges test_g4;;

let test_g5 = MyWeightedGraph.remove_edge "test" "destination" test_g4;;
MyWeightedGraph.is_empty test_g5;;
MyWeightedGraph.succs "test" test_g5;;
MyWeightedGraph.nodes test_g5;;
MyWeightedGraph.edges test_g5;;

(* Fonction pour afficher un graphe étant donné le graphe [g] et une fonction
   [fold_fun] permettant de réaliser le fold_edge, selon le caractère
   orienté ou non du graphe *)
let output_graph fold_fun separator g =
    let nodes_to_string = MyWeightedGraph.fold_node (fun n acc -> n::acc) g [] in
    let edges_to_string = fold_fun (
        fun src dst w acc -> (src^" "^separator^" "^dst^" [Poids : "^string_of_int(w)^"]")::acc
    ) g [] in
    print_string "* Noeuds du graphe [g] : ";
    List.iter (fun s -> print_string (s^" ")) (List.rev nodes_to_string); print_newline (); print_newline ();
    print_endline "* Arêtes du graphe [g] : ";
    List.iter (fun s -> print_endline s) (List.rev edges_to_string);;

(* Test de graphe orienté pondéré *)

print_endline "=== Test de graphe orienté pondéré ==="; print_newline ();;
let directed_graph = MyWeightedGraph.add_edge "n1" "n2" 20 (MyWeightedGraph.add_edge "n1" "n3" 40 MyWeightedGraph.empty);;
let directed_graph = MyWeightedGraph.add_edge "n2" "n3" 5 (MyWeightedGraph.add_edge "n2" "n4" 10 directed_graph);;
output_graph MyWeightedGraph.fold_edge "->" directed_graph; print_newline ();;

(* Test de graphe non orienté pondéré *)

print_endline "=== Test de graphe non orienté pondéré ==="; print_newline ();;
let undirected_graph = MyWeightedGraph.add_undirected_edge "n1" "n2" 20 (MyWeightedGraph.add_edge "n1" "n3" 40 MyWeightedGraph.empty);;
let undirected_graph = MyWeightedGraph.add_undirected_edge "n2" "n3" 5 (MyWeightedGraph.add_edge "n2" "n4" 10 undirected_graph);;
output_graph MyWeightedGraph.fold_undirected_edge "-" undirected_graph; print_newline ();;
