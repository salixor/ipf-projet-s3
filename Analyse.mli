(* [analyse_file_1 file] ouvre le fichier [file] et retourne la liste des
   transitions et les points de départ et d'arrivée décrits dans ce fichier

   Exception [Sys_error msg] si le fichier n'est pas accessible
   Exception [Scanf.Scan_failure msg] si le fichier n'est pas au bon format *)
val analyse_file_1 : string -> (string * string * int) list * (string * string)

(* [output_sol_1 time mod_list] affiche les solutions pour la phase 1.
      - [time] est le temps du chemin de temps minimal
      - [mod_list] = [mod_1; …; mod_n] est la liste des modules traversés *)
val output_sol_1 : int -> string list -> unit

(* [analyse_file_2 file] ouvre le fichier [file] et retourne la liste des
   transitions et la liste des chemins à parcourir

   Exception [Sys_error msg] si le fichier n'est pas accessible
   Exception [Scanf.Scan_failure msg] si le fichier n'est pas au bon format *)
val analyse_file_2 : string -> (string * string * int) list * string list list

(* [output_sol_2 sol] affiche les solutions pour la phase 2.
      - [sol] = [sol_1; …; sol_n] est la liste de la solution, où chaque [sol_i]
        est de la forme (path,times) :
            - path est le chemin demandé
            - times est la liste des temps de départ des différents modules *)
val output_sol_2 : (string list * int list) list -> unit
