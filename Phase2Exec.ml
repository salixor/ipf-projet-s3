(* ========================================================================== *)
(* ======================== Exécution de la phase 2 ========================= *)
(* ========================================================================== *)

(* Récupération du nom de fichier fourni en argument du programme *)
let filename =
    if Array.length Sys.argv > 1 then Sys.argv.(1)
    else failwith "Merci de fournir un nom de fichier";;
print_endline ("Ouverture du fichier '"^filename^"' …"); print_newline ();;

(* Affichage du graphe associé au fichier passé en argument du programme *)
ModuleGraph.output_graph (Phase2.file_to_graph filename);;

(* Affichage des chemins des utilisateurs associés au fichier passé en argument *)
print_newline (); ModuleGraph.output_chemins (Phase2.file_to_chemins filename);;

(* Affichage de la solution associée au fichier passé en argument du programme *)
print_newline (); Phase2.affichage_phase2 filename;;
